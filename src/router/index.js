import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
// 登录注册忘记密码组件
const Login = () => import('../views/Login.vue')
const Reg = () => import('../views/Reg.vue')
const Forget = () => import('../views/Forget.vue')
// 首页
const Index = () => import('../views/change/Index.vue')
const Template1 = () => import('../views/change/Template1.vue')
// 帖子详情组件
const Detail = () => import('../components/contents/Detail.vue')
Vue.use(VueRouter)
const routes = [
  // 首页
  {
    path: '/',
    component: Home,
    children: [
      {
        path: '/',
        name: 'index',
        component: Index
      },
      {
        path: 'index/:catalog', // 路由传参
        name: 'catalog',
        component: Template1
      }
    ]
  },
  // 登录
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  // 注册
  {
    path: '/reg',
    name: 'reg',
    component: Reg
  },
  // 找回密码
  {
    path: '/forget',
    name: 'forget',
    component: Forget
  },
  // 帖子详情
  {
    path: '/detail/:tid',
    name: 'detail',
    component: Detail,
    props: true
  }
]

const router = new VueRouter({
  routes,
  linkExactActiveClass: 'layui-this'// 选中颜色
})

export default router
