export default {
  // http://localhost:3000
  // http://api.dev.toimc.com:22000
  // https://devapi.frontblog.top
  // http://localhost:12006
  dev: 'http://api.dev.toimc.com:22000',
  pro: 'https://api.frontblog.top'
}
