import qs from 'qs'
import axios from '../utils/request'
// 获取文章
const getList = (options) => {
  return axios.get('/public/list?' + qs.stringify(options))
}
// 温馨通道
const getTips = () => {
  return axios.get('/public/tips')
}
// 本周热议
const getTop = () => {
  return axios.get('/public/topweek')
}
// 友情链接
const getLinks = () => {
  return axios.get('/public/links')
}
export { getList, getTips, getTop, getLinks }
