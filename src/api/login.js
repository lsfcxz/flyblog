import axios from '../utils/request'
// 验证码
const getCode = (sid) => {
  return axios.get('/public/getCaptcha', {
    params: {
      sid: sid
    }
  })
}
// 重置密码
const forget = async (options) => {
  return axios.post('/login/forget', { ...options })
}
/**
 * 注册接口
 * @param {} regInfo 用户注册信息
 */
const reg = (regInfo) => {
  return axios.post('/login/reg', {
    ...regInfo
  })
}
// 登录接口
const login = (loginInfo) => {
  return axios.post('/login/login', {
    ...loginInfo
  })
}
export { getCode, forget, reg, login }
