import config from '@/config/index'
import HttpRequest from './axios'
const baseUrl = process.env.NODE_ENV !== 'production' ? config.dev : config.pro
const axios = new HttpRequest(baseUrl)
export default axios
